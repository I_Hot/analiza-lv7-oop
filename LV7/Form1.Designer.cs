﻿namespace LV7
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PB = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.WinX = new System.Windows.Forms.Label();
            this.TIE = new System.Windows.Forms.Label();
            this.WinO = new System.Windows.Forms.Label();
            this.XXX = new System.Windows.Forms.RadioButton();
            this.OOO = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.igracX = new System.Windows.Forms.TextBox();
            this.igracO = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.PB)).BeginInit();
            this.SuspendLayout();
            // 
            // PB
            // 
            this.PB.BackColor = System.Drawing.Color.Black;
            this.PB.Location = new System.Drawing.Point(75, 150);
            this.PB.Name = "PB";
            this.PB.Size = new System.Drawing.Size(300, 300);
            this.PB.TabIndex = 0;
            this.PB.TabStop = false;
            this.PB.Click += new System.EventHandler(this.PB_Click_1);
            this.PB.MouseUp += new System.Windows.Forms.MouseEventHandler(this.PB_MouseUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.White;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(199, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(44, 25);
            this.label3.TabIndex = 3;
            this.label3.Text = "TIE";
            // 
            // WinX
            // 
            this.WinX.AutoSize = true;
            this.WinX.BackColor = System.Drawing.Color.White;
            this.WinX.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WinX.Location = new System.Drawing.Point(70, 78);
            this.WinX.Name = "WinX";
            this.WinX.Size = new System.Drawing.Size(24, 25);
            this.WinX.TabIndex = 4;
            this.WinX.Text = "0";
            // 
            // TIE
            // 
            this.TIE.AutoSize = true;
            this.TIE.BackColor = System.Drawing.Color.White;
            this.TIE.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TIE.Location = new System.Drawing.Point(207, 78);
            this.TIE.Name = "TIE";
            this.TIE.Size = new System.Drawing.Size(24, 25);
            this.TIE.TabIndex = 5;
            this.TIE.Text = "0";
            // 
            // WinO
            // 
            this.WinO.AutoSize = true;
            this.WinO.BackColor = System.Drawing.Color.White;
            this.WinO.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WinO.Location = new System.Drawing.Point(343, 78);
            this.WinO.Name = "WinO";
            this.WinO.Size = new System.Drawing.Size(24, 25);
            this.WinO.TabIndex = 6;
            this.WinO.Text = "0";
            // 
            // XXX
            // 
            this.XXX.AutoSize = true;
            this.XXX.Location = new System.Drawing.Point(165, 38);
            this.XXX.Name = "XXX";
            this.XXX.Size = new System.Drawing.Size(14, 13);
            this.XXX.TabIndex = 7;
            this.XXX.TabStop = true;
            this.XXX.UseVisualStyleBackColor = true;
            // 
            // OOO
            // 
            this.OOO.AutoSize = true;
            this.OOO.Location = new System.Drawing.Point(264, 38);
            this.OOO.Name = "OOO";
            this.OOO.Size = new System.Drawing.Size(14, 13);
            this.OOO.TabIndex = 8;
            this.OOO.TabStop = true;
            this.OOO.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(72, 112);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(306, 27);
            this.button1.TabIndex = 9;
            this.button1.Text = "START / RESTART";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // igracX
            // 
            this.igracX.Location = new System.Drawing.Point(50, 39);
            this.igracX.Name = "igracX";
            this.igracX.Size = new System.Drawing.Size(109, 20);
            this.igracX.TabIndex = 10;
            // 
            // igracO
            // 
            this.igracO.Location = new System.Drawing.Point(284, 39);
            this.igracO.Name = "igracO";
            this.igracO.Size = new System.Drawing.Size(109, 20);
            this.igracO.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MidnightBlue;
            this.ClientSize = new System.Drawing.Size(434, 461);
            this.Controls.Add(this.igracO);
            this.Controls.Add(this.igracX);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.OOO);
            this.Controls.Add(this.XXX);
            this.Controls.Add(this.WinO);
            this.Controls.Add(this.TIE);
            this.Controls.Add(this.WinX);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.PB);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.PB)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label WinX;
        private System.Windows.Forms.Label TIE;
        private System.Windows.Forms.Label WinO;
        private System.Windows.Forms.RadioButton XXX;
        private System.Windows.Forms.RadioButton OOO;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox igracX;
        private System.Windows.Forms.TextBox igracO;
    }
}

