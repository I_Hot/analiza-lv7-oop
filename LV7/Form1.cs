﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LV7
{
    public partial class Form1 : Form
    {
        Graphics g;
        Pen p = new Pen(Color.White, 5);
        bool Flag = true;
        int counter1 = 0;
        int counter2 = 0;
        int counter3 = 0;
        bool WIN = false;
        int t = 0;
        
        int brojac = 0;
        
        public Form1()
        {
            InitializeComponent();
            g = PB.CreateGraphics();


        }

        int[] Array = { 2, 2, 2, 2, 2, 2, 2, 2, 2 };


        class Circle
        {
            int r;
            public Circle()
            {
                r = 65;
            }
            public void draw(Graphics g, Pen p, int x, int y)
            {
                g.DrawEllipse(p, x, y, r, r);
            }
        }
        class Cross
        {
            
            public Cross()
            {
                
            }
            public void draw(Graphics g, Pen p, int x, int y)
            {
                g.DrawLine(p, x, y, x + 65, y + 65);
                g.DrawLine(p, x+65, y, x , y + 65);
            }
        }
        private void PB_MouseUp(object sender, MouseEventArgs e)
        {
            if (Flag)
            {
                if (e.X >= 0 && e.Y >= 0 && e.X < 100 && e.Y < 100)
                {
                    if (Array[0] == 2)
                    {
                        Circle c = new Circle();
                        c.draw(g, p, 15, 15);
                        Array[0] = 1;
                         
                    }
                }
                if (e.X >= 100 && e.Y >= 0 && e.X < 200 && e.Y < 100)
                {
                    if (Array[1] == 2)
                    {
                        Circle c = new Circle();
                        c.draw(g, p, 100 + 15, 15);
                        Array[1] = 1;
                         
                    }
                }
                if (e.X >= 200 && e.Y >= 0 && e.X < 300 && e.Y < 100)
                {
                    if (Array[2] == 2)
                    {
                        Circle c = new Circle();
                        c.draw(g, p, 200 + 15, 15);
                        Array[2] = 1;
                         
                    }
                }



                if (e.X >= 0 && e.Y >= 100 && e.X < 100 && e.Y < 200)
                {
                    if (Array[3] == 2)
                    {
                        Circle c = new Circle();
                        c.draw(g, p, 15, 100 + 15);
                        Array[3] = 1;
                         
                    }
                }
                if (e.X >= 100 && e.Y >= 100 && e.X < 200 && e.Y < 200)
                {
                    if (Array[4] == 2)
                    {
                        Circle c = new Circle();
                        c.draw(g, p, 100 + 15, 100 + 15);
                        Array[4] = 1;
                         
                    }
                }
                if (e.X >= 200 && e.Y >= 100 && e.X < 300 && e.Y < 200)
                {
                    if (Array[5] == 2)
                    {
                        Circle c = new Circle();
                        c.draw(g, p, 200 + 15, 100 + 15);
                        Array[5] = 1;
                         
                    }
                }





                if (e.X >= 0 && e.Y >= 200 && e.X < 100 && e.Y < 300)
                {
                    if (Array[6] == 2)
                    {
                        Circle c = new Circle();
                        c.draw(g, p, 15, 200 + 15);
                        Array[6] = 1;
                         
                    }
                }
                if (e.X >= 100 && e.Y >= 200 && e.X < 200 && e.Y < 300)
                {
                    if (Array[7] == 2)
                    {
                        Circle c = new Circle();
                        c.draw(g, p, 100 + 15, 200 + 15);
                        Array[7] = 1;
                         
                    }
                }
                if (e.X >= 200 && e.Y >= 200 && e.X < 300 && e.Y < 300)
                {
                    if (Array[8] == 2)
                    {
                        Circle c = new Circle();
                        c.draw(g, p, 200 + 15, 200 + 15);
                        Array[8] = 1;
                         
                    }
                }
                 
            }


            else
            {
                if (e.X >= 0 && e.Y >= 0 && e.X < 100 && e.Y < 100)
                {
                    if (Array[0] == 2)
                    {
                        Cross x = new Cross();
                        x.draw(g, p, 15, 15);
                        Array[0] = 0;
                         
                    }
                }
                if (e.X >= 100 && e.Y >= 0 && e.X < 200 && e.Y < 100)
                {
                    if (Array[1] == 2)
                    {
                        Cross x = new Cross();
                        x.draw(g, p, 100 + 15, 15);
                        Array[1] = 0;
                         
                    }
                }
                if (e.X >= 200 && e.Y >= 0 && e.X < 300 && e.Y < 100)
                {
                    if (Array[2] == 2)
                    {
                        Cross x = new Cross();
                        x.draw(g, p, 200 + 15, 15);
                        Array[2] = 0;
                         
                    }
                }



                if (e.X >= 0 && e.Y >= 100 && e.X < 100 && e.Y < 200)
                {
                    if (Array[3] == 2)
                    {
                        Cross x = new Cross();
                        x.draw(g, p, 15, 100 + 15);
                        Array[3] = 0;
                         
                    }
                }
                if (e.X >= 100 && e.Y >= 100 && e.X < 200 && e.Y < 200)
                {
                    if (Array[4] == 2)
                    {
                        Cross x = new Cross();
                        x.draw(g, p, 100 + 15, 100 + 15);
                        Array[4] = 0;
                         
                    }
                }
                if (e.X >= 200 && e.Y >= 100 && e.X < 300 && e.Y < 200)
                {
                    if (Array[5] == 2)
                    {
                        Cross x = new Cross();
                        x.draw(g, p, 200 + 15, 100 + 15);
                        Array[5] = 0;
                         
                    }
                }





                if (e.X >= 0 && e.Y >= 200 && e.X < 100 && e.Y < 300)
                {
                    if (Array[6] == 2)
                    {
                        Cross x = new Cross();
                        x.draw(g, p, 15, 200 + 15);
                        Array[6] = 0;
                         
                    }
                }
                if (e.X >= 100 && e.Y >= 200 && e.X < 200 && e.Y < 300)
                {
                    if (Array[7] == 2)
                    {
                        Cross x = new Cross();
                        x.draw(g, p, 100 + 15, 200 + 15);
                        Array[7] = 0;
                         
                    }
                }
                if (e.X >= 200 && e.Y >= 200 && e.X < 300 && e.Y < 300)
                {
                    if (Array[8] == 2)
                    {
                        Cross x = new Cross();
                        x.draw(g, p, 200 + 15, 200 + 15);
                        Array[8] = 0;
                         
                    }
                }


            }
            Flag = !Flag;
            if (Array[0] == Array[1] && Array[0] == Array[2]) { if (Array[0] == 0) { counter1++; MessageBox.Show("X Wins"); g.Clear(Color.Black); WIN = true; } if (Array[0] == 1) { counter2++; MessageBox.Show("O Wins"); g.Clear(Color.Black); WIN = true; } }
            if (Array[0] == Array[3] && Array[0] == Array[6]) { if (Array[0] == 0) { counter1++; MessageBox.Show("X Wins"); g.Clear(Color.Black); WIN = true; } if (Array[0] == 1) { counter2++; MessageBox.Show("O Wins"); g.Clear(Color.Black); WIN = true; } }
            if (Array[0] == Array[4] && Array[0] == Array[8]) { if (Array[0] == 0) { counter1++; MessageBox.Show("X Wins"); g.Clear(Color.Black); WIN = true; } if (Array[0] == 1) { counter2++; MessageBox.Show("O Wins"); g.Clear(Color.Black); WIN = true; } }
            if (Array[1] == Array[4] && Array[1] == Array[7]) { if (Array[1] == 0) { counter1++; MessageBox.Show("X Wins"); g.Clear(Color.Black); WIN = true; } if (Array[1] == 1) { counter2++; MessageBox.Show("O Wins"); g.Clear(Color.Black); WIN = true; } }
            if (Array[2] == Array[5] && Array[2] == Array[8]) { if (Array[2] == 0) { counter1++; MessageBox.Show("X Wins"); g.Clear(Color.Black); WIN = true; } if (Array[2] == 1) { counter2++; MessageBox.Show("O Wins"); g.Clear(Color.Black); WIN = true; } }
            if (Array[6] == Array[4] && Array[6] == Array[2]) { if (Array[6] == 0) { counter1++; MessageBox.Show("X Wins"); g.Clear(Color.Black); WIN = true; } if (Array[6] == 1) { counter2++; MessageBox.Show("O Wins"); g.Clear(Color.Black); WIN = true; } }
            if (Array[3] == Array[4] && Array[3] == Array[5]) { if (Array[3] == 0) { counter1++; MessageBox.Show("X Wins"); g.Clear(Color.Black); WIN = true; } if (Array[3] == 1) { counter2++; MessageBox.Show("O Wins"); g.Clear(Color.Black); WIN = true; } }
            if (Array[6] == Array[7] && Array[6] == Array[8]) { if (Array[6] == 0) { counter1++; MessageBox.Show("X Wins"); g.Clear(Color.Black); WIN = true; } if (Array[6] == 1) { counter2++; MessageBox.Show("O Wins"); g.Clear(Color.Black); WIN = true; } }
            if (WIN == false)
            {
                for (int i = 0; i < 9; i++)
                {
                    if (Array[i] == 2)
                    {
                        t++;
                    }
                }
                if (t == 0) {
                    counter3++;
                    MessageBox.Show("Draw");
                    g.Clear(Color.Black);
                    g.DrawLine(p, 0, 0, 300, 0);
                    g.DrawLine(p, 0, 0, 0, 300);
                    g.DrawLine(p, 300, 0, 300, 300);
                    g.DrawLine(p, 0, 300, 300, 300);

                    g.DrawLine(p, 100, 0, 100, 300);
                    g.DrawLine(p, 200, 0, 200, 300);

                    g.DrawLine(p, 0, 100, 300, 100);
                    g.DrawLine(p, 0, 200, 300, 200);
                    for (int i = 0; i <= 8; i++)
                    {
                        Array[i] = 2;
                    }

                }

            }
            t = 0;
            
           
            WinX.Text = counter1.ToString();
            WinO.Text = counter2.ToString();
            TIE.Text = counter3.ToString();


            
            
            if (WIN)
            {
                g.DrawLine(p, 0, 0, 300, 0);
                g.DrawLine(p, 0, 0, 0, 300);
                g.DrawLine(p, 300, 0, 300, 300);
                g.DrawLine(p, 0, 300, 300, 300);

                g.DrawLine(p, 100, 0, 100, 300);
                g.DrawLine(p, 200, 0, 200, 300);

                g.DrawLine(p, 0, 100, 300, 100);
                g.DrawLine(p, 0, 200, 300, 200);
                for (int i = 0; i <= 8; i++)
                {
                    Array[i] = 2;
                }

                
                WIN = false;
            }
          

        }
            

        

        private void process1_Exited(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            g.Clear(Color.Black);
            g.DrawLine(p, 0, 0, 300, 0);
            g.DrawLine(p, 0, 0, 0, 300);
            g.DrawLine(p, 300, 0, 300, 300);
            g.DrawLine(p, 0, 300, 300, 300);

            g.DrawLine(p, 100, 0, 100, 300);
            g.DrawLine(p, 200, 0, 200, 300);

            g.DrawLine(p, 0, 100, 300, 100);
            g.DrawLine(p, 0, 200, 300, 200);

            counter1 = 0;
            counter2 = 0;
            counter3 = 0;
            WinX.Text = counter1.ToString();
            WinO.Text = counter2.ToString();
            TIE.Text = counter3.ToString();
            for (int i = 0; i <= 8; i++)
            {
                Array[i] = 2;
            }

            Flag = true;


        }

        private void PB_Click(object sender, EventArgs e)
        {

        }

        private void PB_Click_1(object sender, EventArgs e)
        {
            if (XXX.Checked == true)
            {
                OOO.Checked = true;
                XXX.Checked = false;
            }
            else
            {
                OOO.Checked = false;
                XXX.Checked = true;

            }
        }
    }
}
